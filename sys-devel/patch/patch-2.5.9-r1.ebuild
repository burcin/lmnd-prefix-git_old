# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-devel/patch/patch-2.5.9-r1.ebuild,v 1.18 2008/03/18 12:34:29 vapier Exp $

inherit flag-o-matic eutils

DESCRIPTION="Utility to apply diffs to files"
HOMEPAGE="http://www.gnu.org/software/patch/patch.html"
#SRC_URI="mirror://gnu/patch/${P}.tar.gz"
#Using own mirrors until gnu has md5sum and all packages up2date
SRC_URI="mirror://gentoo/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~ppc-aix ~x64-freebsd ~x86-freebsd ~hppa-hpux ~ia64-hpux ~x86-interix ~amd64-linux ~ia64-linux ~x86-linux ~ppc-macos ~x64-macos ~x86-macos ~m68k-mint ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE="static"

DEPEND=""

src_unpack() {
	unpack ${A}
	cd "${S}"
	if type -p patch > /dev/null ; then
		epatch "${FILESDIR}"/patch-2.5.9-cr-stripping.patch
	fi
}

src_compile() {
	strip-flags
	use kernel_linux && append-flags -DLINUX
	append-flags -D_XOPEN_SOURCE=500

	# From the Solaris lfcompile(5) manpage:
	# Applications wishing to access   fseeko()  and  ftello()  as
	# well as the POSIX and X/Open specification-conforming inter-
	# faces should define the macro _LARGEFILE_SOURCE to be 1  and
	# set  whichever feature test macros are appropriate to obtain
	# the desired environment (see standards(5)).
	[[ ${CHOST} = *-solaris* ]] && append-flags -D_LARGEFILE_SOURCE=1

	use static && append-ldflags -static

	local myconf=""
	[[ ${USERLAND} == "BSD" ]] && [[ ${PREFIX%/} != "" ]] \
		&& myconf="--program-prefix=g"
	ac_cv_sys_long_file_names=yes econf ${myconf} || die

	emake || die "emake failed"
}

src_install() {
	einstall || die
	dodoc AUTHORS ChangeLog NEWS README
}
