# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="LELA is a C++ template-library for computations in (normally) exact linear algebra."
HOMEPAGE="http://www.singular.uni-kl.de/lela/index.htm"
SRC_URI="http://www.singular.uni-kl.de/lela/lela-0.1.0.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~mips ~sparc ~x86"
IUSE=""

inherit eutils 

src_prepare() {
   for file in "${FILESDIR}"/${PV}/*.patch
   do
     epatch "${file}"
   done
}

src_compile() {
    src_prepare
    econf
    emake || die
}

src_install() {
    emake DESTDIR="${D}" install || die
}
